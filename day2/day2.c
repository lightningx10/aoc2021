#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#define LINE_LEN 100

int getLine(char *);
int isNumber(char *);
int toInt(char *);
int sum(int *, int);
int isCommand(char *);

int main() {
  char *buffer = malloc(LINE_LEN * sizeof(char));
  int hoz = 0, dep = 0;
  int newDep = 0, newAim = 0;
  while (getLine(buffer)) {
    int spaceLoc;
    if ((spaceLoc = isCommand(buffer)) == 0) {
      fprintf(stderr, "Invalid command entered");
      exit(1);
    }
    switch (buffer[0]) {
    case 'f':
      if (isNumber(buffer + spaceLoc + 1)) {
        int x = toInt(buffer + spaceLoc + 1);
        hoz += x;
        newDep += x * newAim;
      }
      break;
    case 'd':
      if (isNumber(buffer + spaceLoc + 1)) {
        int x = toInt(buffer + spaceLoc + 1);
        dep += x;
        newAim += x;
      }
      break;
    case 'u':
      if (isNumber(buffer + spaceLoc + 1)) {
        int x = toInt(buffer + spaceLoc + 1);
        dep -= x;
        newAim -= x;
      }
      break;
    default:
      fprintf(stderr, "Invalid command entered");
      break;
    }
  }
  printf("Simple Horizontal Pos: %d\nSimple Depth: %d\n", hoz, dep);
  printf("Complex Horizontal Pos: %d\nComplex Depth: %d\n", hoz, newDep);
  return 0;
}

int sum(int *array, int size) {
  int sum = 0;
  for (int i = 0; i < size; i++) {
    sum += array[i];
  }
  return sum;
}

int getLine(char *buffer) {
  char *in = malloc(sizeof(char));
  for (int i = 0; i < LINE_LEN && *in != '\n'; i++) {
    if (i == LINE_LEN - 1) {
      fprintf(stderr, "LINE EXCEEDS 100 CHARS\n");
      exit(1);
    }
    if (read(STDIN_FILENO, in, 1) == 0) {
      printf("Reached EOF\n");
      return 0;
    }
    if (*in == '\n') {
      buffer[i] = '\0';
      return 1;
    } else {
      buffer[i] = *in;
    }
  }
  return 0;
}

int isCommand(char *string) {
  int len = strlen(string);
  int numSpaces = 0;
  int spaceIndex = 0;
  for (int i = 0; i < len; i++) {
    if (string[i] == ' ') {
      numSpaces++;
      spaceIndex = i;
    }
  }
  if (numSpaces != 1) {
    return 0;
  } else {
    if (string[0] != 'f' && string[0] != 'd' && string[0] != 'u') {
      return 0;
    }
  }
  return spaceIndex;
}

int isNumber(char *string) {
  int isNumber = 1;
  int len = strlen(string);
  for (int i = 0; i < len; i++) {
    isNumber =
        isNumber && ((string[i] >= '0' && string[i] <= '9') || string[i] == 13);
    if (!isNumber) {
      printf("failed at i=%d, len=%d\n", i, len);
      printf("%c, %d\n", string[i], string[i]);
    }
  }
  return isNumber;
}

int toInt(char *string) {
  int len;
  int out = 0;
  for (len = 0; string[len] >= '0' && string[len] <= '9'; len++) {
  }
  for (int i = 0; i < len; i++) {
    out += (string[i] - '0') * pow(10, len - 1 - i);
  }
  return out;
}
