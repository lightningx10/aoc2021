#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#define LINE_LEN 14
#define BASE_LINES_STORED 1000

int binArrayToInt(int *, int);
int **gen2DArr(int);
int getLine(int *);
int *getOGenVal(int **, int, int);
int *getScrubVal(int **, int, int);
int *leastCommonExclude(int **, int *, int, int);
int *mostCommon(int **, int, int);
int *mostCommonExclude(int **, int *, int, int);
int *onesComp(int *, int);
void printArray(int *, int);

int main() {
  int **lines = gen2DArr(BASE_LINES_STORED);
  int i = 0;
  while (getLine(lines[i])) {
    if (i >= BASE_LINES_STORED) {
      fprintf(stderr, "Overfilled buffer\n");
      exit(1);
    }
    i++;
  }
  int *gamma = mostCommon(lines, 12, 1000);
  int *epsilon = onesComp(gamma, 12);
  printArray(gamma, 12);
  printArray(epsilon, 12);
  printf("Gamma: %d\n", binArrayToInt(gamma, 12));
  printf("Epsilon: %d\n", binArrayToInt(epsilon, 12));
  int *oGen = getOGenVal(lines, 12, BASE_LINES_STORED);
  int *scrubVal = getScrubVal(lines, 12, BASE_LINES_STORED);
  printArray(oGen, 12);
  printArray(scrubVal, 12);
  printf("Oxygen Generator Rating: %d\n", binArrayToInt(oGen, 12));
  printf("CO2 Scrub Value Rating: %d\n", binArrayToInt(scrubVal, 12));
  free(lines);
  return 0;
}

int sum(int *array, int len) {
  int sum = 0;
  for (int i = 0; i < len; i++) {
    sum += array[i];
  }
  return sum;
}

int *getOGenVal(int **table, int rowLen, int rowNum) {
  int *removed = malloc(rowNum * sizeof(int));
  for (int i = 0; i < rowLen; i++) {
    if (sum(removed, rowNum) < (rowNum - 1)) {
      int num = mostCommonExclude(table, removed, rowLen, rowNum)[i];
      for (int j = 0; j < rowNum; j++) {
        if (table[j][i] != num) {
          removed[j] = 1;
        }
      }
    }
  }
  for (int i = 0; i < rowNum; i++) {
    if (!removed[i]) {
      return table[i];
    }
  }
  fprintf(stderr, "getOGenVal has failed!\n");
  exit(1);
  return table[0];
}

int *getScrubVal(int **table, int rowLen, int rowNum) {
  int *removed = malloc(rowNum * sizeof(int));
  for (int i = 0; i < rowLen; i++) {
    if (sum(removed, rowNum) < (rowNum - 1)) {
      int num = !mostCommonExclude(table, removed, rowLen, rowNum)[i];
      for (int j = 0; j < rowNum; j++) {
        if (table[j][i] == num) {
          removed[j] = 1;
        }
      }
    }
  }
  for (int i = 0; i < rowNum; i++) {
    if (!removed[i]) {
      return table[i];
    }
  }
  fprintf(stderr, "getScrubVal has failed!\n");
  exit(1);
  return table[0];
}

int binArrayToInt(int *array, int len) {
  int ret = 0;
  for (int i = 0; i < len; i++) {
    ret += array[i] << (len - 1 - i);
  }
  return ret;
}

int *onesComp(int *array, int len) {
  int *ret = malloc(len * sizeof(int));
  for (int i = 0; i < len; i++) {
    ret[i] = !array[i];
  }
  return ret;
}

int *mostCommonExclude(int **table, int *exclude, int rowLen, int rowNum) {
  int *ret = malloc(rowLen * sizeof(int));
  int *ones = malloc(rowLen * sizeof(int));
  for (int i = 0; i < rowLen; i++) {
    ret[i] = 0;
    ones[i] = 0;
  }
  for (int i = 0; i < rowNum; i++) {
    for (int j = 0; j < rowLen; j++) {
      if (table[i][j] == 1 && !exclude[i]) {
        ones[j]++;
      }
    }
  }
  int excludeNum = 0;
  for (int i = 0; i < rowNum; i++) {
    if (exclude[i]) {
      excludeNum++;
    }
  }
  for (int i = 0; i < rowLen; i++) {
    if (ones[i] > (rowNum - excludeNum) / 2) {
      ret[i] = 1;
    }
  }
  return ret;
}

int *mostCommon(int **table, int rowLen, int rowNum) {
  int *ret = malloc(rowLen * sizeof(int));
  int *ones = malloc(rowLen * sizeof(int));
  for (int i = 0; i < rowLen; i++) {
    ret[i] = 0;
    ones[i] = 0;
  }
  for (int i = 0; i < rowNum; i++) {
    for (int j = 0; j < rowLen; j++) {
      if (table[i][j] == 1) {
        ones[j]++;
      }
    }
  }
  for (int i = 0; i < rowLen; i++) {
    if (ones[i] > rowNum / 2) {
      ret[i] = 1;
    }
  }
  return ret;
}

void printArray(int *array, int arrayLen) {
  printf("[");
  for (int i = 0; i < arrayLen; i++) {
    printf("%d", array[i]);
    if (i != arrayLen - 1) {
      printf(", ");
    }
  }
  printf("]\n");
}

int **gen2DArr(int len) {
  int **ret = malloc(len * sizeof(int *));
  for (int i = 0; i < len; i++) {
    ret[i] = malloc(LINE_LEN * sizeof(int));
  }
  return ret;
}

int getLine(int *array) {
  char *in = malloc(sizeof(char));
  for (int i = 0; i < LINE_LEN && *in != '\n'; i++) {
    if (read(STDIN_FILENO, in, 1) == 0) {
      free(in);
      printf("Reached EOF\n");
      return 0;
    }
    if (*in == '\n') {
      free(in);
      return 1;
    } else if (*in == '0' || *in == '1') {
      array[i] = *in - '0';
    }
  }
  fprintf(stderr, "LINE EXCEEDS %d CHARS\n", LINE_LEN);
  exit(1);
  return 0;
}
