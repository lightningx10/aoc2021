import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Part2 {
    static int LINE_NUM = 12;
    static int LINE_LEN = 5;
    public static void main(String[] args) {
        List<int[]> table = new ArrayList<>();
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        try {
            int[] in;
            for (int i = 0; i < LINE_NUM; i ++) {
                in = toIntArray(bufferedReader.readLine());
                table.add(in);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        int[] oGen = getOGenVal(table);
        if (oGen == null) {
            System.out.println("Oxygen Generator Rating Failed");
            return;
        }
        System.out.println(Arrays.stream(oGen).sum());
        System.out.println(binArrayToInt(oGen));
        printArr(oGen);
        int[] scrubVal = getScrubVal(table);
        if (scrubVal == null) {
            System.out.println("CO2 Scrubbing Generator Rating Failed");
            return;
        }
        System.out.println(Arrays.stream(scrubVal).sum());
        System.out.println(binArrayToInt(scrubVal));
        printArr(scrubVal);
    }

    public static void printArr(int[] array) {
        System.out.printf("[");
        for (int i = 0; i < array.length; i++) {
            System.out.printf("%d", array[i]);
            if (i != array.length - 1) {
                System.out.printf(", ");
            }
        }
        System.out.printf("]\n");
    }

    public static int binArrayToInt(int[] array) {
        int ret = 0;
        for (int i = 0; i < array.length; i++) {
            ret += array[i] << (array.length - 1 - i);
        }
        return ret;
    }

    public static int[] getScrubVal(List<int[]> table) {
        int rowLen = table.get(0).length;
        List<int[]> remaining = new ArrayList<>(table);
        for (int i = 0; i < rowLen; i++) {
            int num = leastCommon(remaining)[i];
            int rowNum = remaining.size();
            for (int j = rowNum - 1; j > 0; j--) {
                if (remaining.get(j)[i] != num && remaining.size() > 1) {
                    remaining.remove(j);
                }
            }
        }
        if (remaining.size() == 1) {
            return remaining.get(0);
        } else {
            return null;
        }
    }

    public static int[] getOGenVal(List<int[]> table) {
        int rowLen = table.get(0).length;
        List<int[]> remaining = new ArrayList<>(table);
        for (int i = 0; i < rowLen; i++) {
            int num = mostCommon(remaining)[i];
            int rowNum = remaining.size();
            for (int j = rowNum - 1; j > 0; j--) {
                if (remaining.get(j)[i] != num && remaining.size() > 1) {
                    remaining.remove(j);
                }
            }
        }
        if (remaining.size() == 1) {
            return remaining.get(0);
        } else {
            return null;
        }
    }

    public static int[] mostCommon(List<int[]> table) {
        int rowLen = table.get(0).length;
        int rowNum = table.size();
        int[] ret = new int[rowLen];
        int[] ones = new int[rowLen];
        for (int i = 0; i < rowLen; i++) {
            ret[i] = 0;
            ones[i] = 0;
        }
        for (int i = 0; i < rowNum; i++) {
            for (int j = 0; j < rowLen; j++) {
                if (table.get(i)[j] == 1) {
                    ones[j]++;
                }
            }
        }
        for (int i = 0; i < rowLen; i++) {
            if (ones[i] > rowNum / 2) {
                ret[i] = 1;
            }
        }
        return ret;
    }

    public static int[] leastCommon(List<int[]> table) {
        int rowLen = table.get(0).length;
        int rowNum = table.size();
        int[] ret = new int[rowLen];
        int[] ones = new int[rowLen];
        for (int i = 0; i < rowLen; i++) {
            ret[i] = 0;
            ones[i] = 0;
        }
        for (int i = 0; i < rowNum; i++) {
            for (int j = 0; j < rowLen; j++) {
                if (table.get(i)[j] == 1) {
                    ones[j]++;
                }
            }
        }
        for (int i = 0; i < rowLen; i++) {
            if (ones[i] < rowNum / 2) {
                ret[i] = 1;
            }
        }
        return ret;
    }

    public static int[] toIntArray(String str) {
        int[] ret = new int[str.length()];
        for (int i = 0; i < str.length(); i ++) {
            ret[i] = str.charAt(i) - '0';
        }
        return ret;
    }
}
