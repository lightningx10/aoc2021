#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#define LINE_LEN 100

int getLine(char *);
int isNumber(char *);
int toInt(char *);
int sum(int *, int);

int main() {
  char *buffer = malloc(LINE_LEN * sizeof(char));
  int temp = -1;
  int inc = 0;
  int incWin = 0;
  int lastThree[] = {0, 0, 0};
  int thisThree[] = {0, 0, 0};
  int i = 0;
  while (getLine(buffer)) {
    if (!isNumber(buffer)) {
      fprintf(stderr, "NOT A NUMBER:\n%s\n", buffer);
      exit(1);
    }
    int num = toInt(buffer);
    if (temp == -1) {
      temp = num;
    } else if (toInt(buffer) > temp) {
      temp = num;
      printf("Increased!\n");
      inc++;
    } else {
      temp = num;
    }
    if (i >= 3) {
      for (int i = 0; i < 3; i++) {
        lastThree[i] = thisThree[i];
      }
    }
    thisThree[i % 3] = num;
    if (i > 3 && sum(thisThree, 3) > sum(lastThree, 3)) {
      printf("Three sum increased!\n");
      incWin++;
    }
    i++;
  }
  printf("Number of times increased:\n%d\n", inc);
  printf("Number of times increased through three sum:\n%d\n", incWin);
  while (getLine(buffer)) {
  }
  return 0;
}

int sum(int *array, int size) {
  int sum = 0;
  for (int i = 0; i < size; i++) {
    sum += array[i];
  }
  return sum;
}

int getLine(char *buffer) {
  char *in = malloc(sizeof(char));
  for (int i = 0; i < LINE_LEN && *in != '\n'; i++) {
    if (i == LINE_LEN - 1) {
      fprintf(stderr, "LINE EXCEEDS 100 CHARS\n");
      exit(1);
    }
    if (read(STDIN_FILENO, in, 1) == 0) {
      printf("Reached EOF\n");
      return 0;
    }
    if (*in == '\n') {
      buffer[i] = '\0';
      return 1;
    } else {
      buffer[i] = *in;
    }
  }
  return 0;
}

int isNumber(char *string) {
  int isNumber = 1;
  int len = strlen(string);
  for (int i = 0; i < len; i++) {
    isNumber =
        isNumber && ((string[i] >= '0' && string[i] <= '9') || string[i] == 13);
    if (!isNumber) {
      printf("failed at i=%d, len=%d\n", i, len);
      printf("%c, %d\n", string[i], string[i]);
    }
  }
  return isNumber;
}

int toInt(char *string) {
  int len;
  int out = 0;
  len = strlen(string);
  for (int i = 0; i < len; i++) {
    out += (string[i] - '0') * pow(10, len - 1 - i);
  }
  return out;
}
